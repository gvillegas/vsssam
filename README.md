A PoC of CVE-2021-36934 written in C++

**Usage: `VSSSAM.exe`**
**EDR looking for ps1 execution? no problem! auto search of VSS using QueryDosDevice function**

![](VSSSAM2.png)
![](VSSSAM1.png)

**References:**
- https://www.helpnetsecurity.com/2021/07/21/cve-2021-36934/
- Windows10 system programming (book of Pavel Yosifovich)

